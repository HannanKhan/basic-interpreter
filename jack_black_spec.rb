require 'rspec'
require_relative 'jack_black'

describe 'interpreter' do

  #initializing interpreter to make it ready for testing
  interpreter = JackBlack.new
  interpreter.init_var(['Let', 'y', '=', '2'])

  it 'should initialize a varibale successfully' do
    expect(interpreter.init_var(['Let', 'x', '=', '5'])).to be 5
  end

  it 'should evaluate a command properly' do
    expect(interpreter.evaluate(['z', '=', 'x', '+', 'y']))
  end

  it 'should display a varibale correctly' do
    expect(interpreter.flush_out(['print', 'z'])).to be 7
  end
end