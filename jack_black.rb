class JackBlack
  def initialize()
    #initialize global variables
    @_gvars = {}
  end

  def run(file)
    #run a script file
    #split script in lines
    File.foreach(file) do |line|
      #tokkenize each line in to command and args

      args = line.split(' ')
      case args[0]
        when 'Let'
          init_var(args)
        when 'print'
          flush_out(args)
        else
          evaluate(args)
      end
    end

  end

  def init_var(args)
    #initialize a variable
    if args.length > 4
      #case the argument are more than 4 means, its of form Let z = x + y
      evaluate args[1..5]
    else
      #default case of type z = x + y
      @_gvars[args[1]] = args[3].to_i
    end
  end

  def flush_out(args)
    #check args[1] is a number
    if !/\A\d+\z/.match args[1]
      print @_gvars[args[1]]
      @_gvars[args[1]]
    else
      print args[1]
      args[1].to_i
    end
  end

  def parse_args(list)
    #gets values of variables incase they are string names
    list.map do |val|
      (!/\A\d+\z/.match val) ? @_gvars[val] : val.to_i
    end
  end

  def calculate(operands, operator)
    #evaluates an expression
    eval("#{operands[0]} #{operator} #{operands[1]}")
  end

  def compute(args)
    #extract arguments from call
    operands = parse_args([args[2], args[4]])

    #calulate the results and return the value
    calculate(operands, args[3])
  end

  def evaluate(args)
    #if variables doesn't exist initialize it with zero
    @_gvars[args[0]] ||= 0
    #evaluate RHS and store result in desired variable
    @_gvars[args[0]] = compute(args)
  end

  def interpret(script)
    #interpret from a string passed
    lines = script.split(';')
    lines.each do |line|
      args = line.split(' ')

      case args[0]
        when 'Let'
          init_var(args)
        when 'print'
          flush_out(args)
        else
          evaluate(args)
      end

    end
  end
end